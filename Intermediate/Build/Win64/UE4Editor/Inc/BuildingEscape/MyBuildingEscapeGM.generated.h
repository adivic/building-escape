// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BUILDINGESCAPE_MyBuildingEscapeGM_generated_h
#error "MyBuildingEscapeGM.generated.h already included, missing '#pragma once' in MyBuildingEscapeGM.h"
#endif
#define BUILDINGESCAPE_MyBuildingEscapeGM_generated_h

#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_RPC_WRAPPERS
#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyBuildingEscapeGM(); \
	friend struct Z_Construct_UClass_AMyBuildingEscapeGM_Statics; \
public: \
	DECLARE_CLASS(AMyBuildingEscapeGM, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/BuildingEscape"), NO_API) \
	DECLARE_SERIALIZER(AMyBuildingEscapeGM)


#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyBuildingEscapeGM(); \
	friend struct Z_Construct_UClass_AMyBuildingEscapeGM_Statics; \
public: \
	DECLARE_CLASS(AMyBuildingEscapeGM, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/BuildingEscape"), NO_API) \
	DECLARE_SERIALIZER(AMyBuildingEscapeGM)


#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyBuildingEscapeGM(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyBuildingEscapeGM) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyBuildingEscapeGM); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyBuildingEscapeGM); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyBuildingEscapeGM(AMyBuildingEscapeGM&&); \
	NO_API AMyBuildingEscapeGM(const AMyBuildingEscapeGM&); \
public:


#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyBuildingEscapeGM(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyBuildingEscapeGM(AMyBuildingEscapeGM&&); \
	NO_API AMyBuildingEscapeGM(const AMyBuildingEscapeGM&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyBuildingEscapeGM); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyBuildingEscapeGM); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyBuildingEscapeGM)


#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_PRIVATE_PROPERTY_OFFSET
#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_12_PROLOG
#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_PRIVATE_PROPERTY_OFFSET \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_RPC_WRAPPERS \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_INCLASS \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_PRIVATE_PROPERTY_OFFSET \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_INCLASS_NO_PURE_DECLS \
	BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BUILDINGESCAPE_API UClass* StaticClass<class AMyBuildingEscapeGM>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BuildingEscape_Source_BuildingEscape_MyBuildingEscapeGM_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
