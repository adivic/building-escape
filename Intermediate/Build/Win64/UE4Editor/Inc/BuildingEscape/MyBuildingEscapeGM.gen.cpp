// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BuildingEscape/MyBuildingEscapeGM.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyBuildingEscapeGM() {}
// Cross Module References
	BUILDINGESCAPE_API UClass* Z_Construct_UClass_AMyBuildingEscapeGM_NoRegister();
	BUILDINGESCAPE_API UClass* Z_Construct_UClass_AMyBuildingEscapeGM();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_BuildingEscape();
// End Cross Module References
	void AMyBuildingEscapeGM::StaticRegisterNativesAMyBuildingEscapeGM()
	{
	}
	UClass* Z_Construct_UClass_AMyBuildingEscapeGM_NoRegister()
	{
		return AMyBuildingEscapeGM::StaticClass();
	}
	struct Z_Construct_UClass_AMyBuildingEscapeGM_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyBuildingEscapeGM_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_BuildingEscape,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyBuildingEscapeGM_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MyBuildingEscapeGM.h" },
		{ "ModuleRelativePath", "MyBuildingEscapeGM.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyBuildingEscapeGM_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyBuildingEscapeGM>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyBuildingEscapeGM_Statics::ClassParams = {
		&AMyBuildingEscapeGM::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_AMyBuildingEscapeGM_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMyBuildingEscapeGM_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyBuildingEscapeGM()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyBuildingEscapeGM_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyBuildingEscapeGM, 2222490457);
	template<> BUILDINGESCAPE_API UClass* StaticClass<AMyBuildingEscapeGM>()
	{
		return AMyBuildingEscapeGM::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyBuildingEscapeGM(Z_Construct_UClass_AMyBuildingEscapeGM, &AMyBuildingEscapeGM::StaticClass, TEXT("/Script/BuildingEscape"), TEXT("AMyBuildingEscapeGM"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyBuildingEscapeGM);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
