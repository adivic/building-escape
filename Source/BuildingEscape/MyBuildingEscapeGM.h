// Copyright Antonio Divic

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyBuildingEscapeGM.generated.h"

/**
 * 
 */
UCLASS()
class BUILDINGESCAPE_API AMyBuildingEscapeGM : public AGameModeBase
{
	GENERATED_BODY()
	
};
